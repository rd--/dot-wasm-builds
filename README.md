# dot-wasm-builds

Builds of <https://github.com/mischnic/dot-svg>.

The API is in terms of three DOM elements:

- textarea.dotWasmInput: graph text input
- button.dotWasmRender: render on click
- div.dotWasmOutput: render to location
